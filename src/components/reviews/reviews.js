import $ from 'jquery';
import 'slick-carousel';

const screenWidth = $(window).width();
$('.reviews-slider').slick({
    dots: false,
    arrows: false,
    slidesToShow: 5,
    slidesToScroll: 5,
});


